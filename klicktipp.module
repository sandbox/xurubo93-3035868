<?php
/**
 * @file
 * The base module for klicktipp.
 */

/**
 * Implements hook_libraries_info().
 */
function klicktipp_libraries_info() {
  $libraries['klicktipp'] = array(
    'name' => 'Klicktipp API',
    'vendor url' => 'https://www.klick-tipp.com/handbuch/klick-tipp-per-api-anbinden',
    'download url' => 'https://www.klick-tipp.com/handbuch/klick-tipp-per-api-anbinden',
    'version callback' => 'klicktipp_get_library_version',
    'files' => array(
      'php' => array(
        'klicktipp.api.inc',
      ),
    ),
  );

  return $libraries;
}

/**
 * The version callback for the library
 *
 * @return mixed|string
 */
function klicktipp_get_library_version() {
  $library = libraries_load('klicktipp');
  $apifile = $library['library path'] . '/klicktipp.api.inc';
  if (file_exists($apifile)) {
    $content = file_get_contents($apifile);
    $match = array();
    if (preg_match('/@version\s+([0-9\.]+)/', $content, $match)) {
      return $match[1];
    }
  }
  return 'n/a';
}

/**
 * Implements hook_menu().
 */
function klicktipp_menu() {
  $items['admin/config/services/klicktipp'] = array(
    'title' => 'Klicktipp',
    'description' => t('Klicktipp Configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => ['klicktipp_configuration_form'],
    'file' => 'includes/klicktipp_configuration.form.inc',
    'access arguments' => ['administer klicktipp'],
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function klicktipp_permission() {
  return array(
    'administer klicktipp' => array(
      'title' => t('Administer Klicktipp'),
      'description' => t('Allows access to Klicktipp configuration.'),
    ),
  );
}

/**
 *
 *
 *
 * @return array|bool|\KlicktippConnector|mixed
 */
function klicktipp_get_connector() {
  $library = libraries_load('klicktipp');
  if ($library['loaded']) {
    $klicktipp_connector = &drupal_static(__FUNCTION__);
    if (!isset($klicktipp_connector)) {
      $klicktipp_connector = new KlicktippConnector();
    }

    return $klicktipp_connector;
  }
  else {
    drupal_set_message($library['error message'], 'error');
    return FALSE;
  }
}

/**
 * We can test the REST Api of klicktipp with a simple login
 *
 * @return bool
 */
function klicktipp_test_api_connection() {
  drupal_static_reset('klicktipp_connector');

  $username = variable_get('klicktipp_api_userid', '');
  $password = variable_get('klicktipp_api_password', '');

  $klicktipp_connector = klicktipp_get_connector();

  if ($klicktipp_connector) {
    if (!(bool) $klicktipp_connector->login($username, $password)) {
      drupal_set_message($klicktipp_connector->get_last_error(), 'error');
      return FALSE;
    }
    else {
      return TRUE;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * As per definition of the API, we have to login before we call a function, so I wrote a wrapper for it.
 *
 * @param \KlicktippConnector $klicktipp_connector
 *
 * @return bool
 */
function klicktipp_connector_login($klicktipp_connector) {
  $username = variable_get('klicktipp_api_userid', '');
  $password = variable_get('klicktipp_api_password', '');
  if (!(bool) $klicktipp_connector->login($username, $password)) {
    watchdog('klicktipp', t($klicktipp_connector->get_last_error()), array(), WATCHDOG_ERROR);
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * This is a wrapper for the logout function
 *
 * @param \KlicktippConnector $klicktipp_connector
 *
 * @return bool
 */
function klicktipp_connector_logout($klicktipp_connector) {
  $logged_out = $klicktipp_connector->logout();
  if ($logged_out) {
    return TRUE;
  }
  else {
    watchdog('klicktipp', $klicktipp_connector->get_last_error(), array(), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * This function creates a contact in the Klicktipp Contact Cloud
 *
 * @param $email
 *
 * @param int $subscription_process_id
 * @param int $tag
 *
 * @return bool
 */
function klicktipp_create_contact($email, $subscription_process_id, $tag, $fields = array()) {
  $klicktipp_connector = klicktipp_get_connector();
  if (klicktipp_connector_login($klicktipp_connector)) {
    $subscriber = $klicktipp_connector->subscribe($email, $subscription_process_id, $tag, $fields);
    if ($subscriber) {
      return TRUE;
    }
    else {
      watchdog('klicktipp', $klicktipp_connector->get_last_error(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  else {
    watchdog('klicktipp', $klicktipp_connector->get_last_error(), array(), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Fetch all manual tags from klicktipp
 *
 * @return mixed
 */
function klicktipp_get_tags() {
  $klicktipp_connector = klicktipp_get_connector();
  if (klicktipp_connector_login($klicktipp_connector)) {
    $tags = $klicktipp_connector->tag_index();
    if (is_array($tags) && !empty($tags)) {
      return $tags;
    }
  }
  else {
    drupal_set_message($klicktipp_connector->get_last_error(), 'error');
    return FALSE;
  }
}

/**
 * Get all defined fields from the contact cloud
 *
 * @return mixed
 */
function klicktipp_get_field_index() {
  $klicktipp_connector = klicktipp_get_connector();
  if (klicktipp_connector_login($klicktipp_connector)) {
    $fields = $klicktipp_connector->field_index();
    klicktipp_connector_logout($klicktipp_connector);
    if (is_array($fields) && !empty($fields)){
      return $fields;
    }
  }
  else {
    watchdog('klicktipp', $klicktipp_connector->get_last_error(), array(), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Filter Klicktipp Tags and build an associated array
 *
 * @return array
 */
/*function klicktipp_get_tags_filtered() {
  $tags = klicktipp_get_tags();
  $tags_filtered = array();
  foreach ($tags as $tagid => $tagname) {
    $tags_filtered[$tagid] = $tagname;
  }
  return $tags_filtered;
}*/

/**
 * Sets an array of tag ids on a specific contact
 *
 * @param $email
 * @param array $tagids
 *
 * @return bool $ret
 */
function klicktipp_add_tags_to_contact($email, $tagids) {
  if (empty($email)) {
    watchdog('klicktipp', t('Email address is not provided'), array(), WATCHDOG_ERROR);
    return FALSE;
  }

  if (empty($tagids)) {
    watchdog('klicktipp', t('Tag Ids are not provided'), array(), WATCHDOG_ERROR);
    return FALSE;
  }
  $klicktipp_connector = klicktipp_get_connector();
  $email_decoded = trim(html_entity_decode($email, ENT_COMPAT, 'UTF-8'));
  if (klicktipp_connector_login($klicktipp_connector)) {
    $ret = $klicktipp_connector->tag($email_decoded, $tagids);
    klicktipp_connector_logout($klicktipp_connector);
    if ($ret) {
      return TRUE;
    }
    else {
      watchdog('klicktipp', $klicktipp_connector->get_last_error(), array('Email' => $email_decoded, 'Tags' => $tagids), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * @return \A|bool
 */
function klicktipp_get_subscription_ids() {
  $klicktipp_connector = klicktipp_get_connector();
  if (klicktipp_connector_login($klicktipp_connector)) {
    $subscription_ids = $klicktipp_connector->subscription_process_index();
    if (is_array($subscription_ids) && !empty($subscription_ids)) {
      return $subscription_ids;
    }
  }
  else {
    drupal_set_message($klicktipp_connector->get_last_error(), 'error');
    return FALSE;
  }
}