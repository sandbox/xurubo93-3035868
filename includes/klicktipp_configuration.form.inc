<?php
/**
 * Created by PhpStorm.
 * User: peterwindholz
 * Date: 2019-01-11
 * Time: 11:42
 */

function klicktipp_configuration_form($form, &$form_state) {
  $form['klicktipp_api_userid'] = array(
    '#type' => 'textfield',
    '#title' => t('Klicktipp UserID'),
    '#default_value' => variable_get('klicktipp_api_userid', ''),
    '#required' => TRUE,
    '#description' => t('Your Klicktipp UserID which you use for your account login'),
    '#attributes' => array(
      'placeholder' => t('Enter your account user ID you use for your Klicktipp account'),
    ),
  );

  $form['klicktipp_api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Klicktipp password'),
    '#default_value' => variable_get('klicktipp_api_password', ''),
    '#required' => TRUE,
    '#description' => t('Your Klicktipp password which you use for your account login'),
  );

  $form['klicktipp_api_test_connection'] = array(
    '#type' => 'button',
    '#value' => t('Test Connection'),
    '#executes_submit_callback' => TRUE,
    '#submit' => array(
      'klicktipp_api_test_connection_submit'
    ),
  );

  return system_settings_form($form);
}

/**
 * Form test submission handler for klicktipp_configuration_form().
 * 
 * @param $form
 * @param $form_state
 */
function klicktipp_api_test_connection_submit($form, &$form_state) {
  variable_set('klicktipp_api_userid', trim($form_state['values']['klicktipp_api_userid']));
  variable_set('klicktipp_api_password', trim($form_state['values']['klicktipp_api_password']));
  
  //Test the connection to klicktipp
  if (klicktipp_test_api_connection()) {
    drupal_set_message(t('Connection to Klicktipp successfully established'), 'status');
  }
  else {
    drupal_set_message(t('Connection to Klicktipp failed'), 'error');
  }
 }